const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
var response = {
	"status":"",
	"attachment":""
}

function compilation(code,lang,_callback) {

	const filepath = "/tmp/code."+lang;

	fs.writeFile(filepath,code,function(err){
		if(err) throw err;
	})

	const { exec } = require("child_process");

	var cmd;
	
	if(lang=="c"){

		cmd = "gcc /tmp/code.c -o /tmp/code.out";

	}
	else if(lang=="cpp"){

		cmd = "g++ /tmp/code.cpp -o /tmp/code.out";
	
	}

	exec(cmd,
		function(err,stdout,stderr) {
			if(err !== null){
				response["status"] = "Compilation Error";
				response["attachment"] = stderr;
			}
			else{
				if(stderr.length > 0){
					response["status"] = "Compilation Error";
					response["attachment"] = stderr;
				}
				else{
					response["status"] = "Compilation Successful";
				}
			}
			try{
				fs.unlinkSync(filepath);
			}
			catch(err1){
				console.log('Error: '+err1);
			}
			try {
				fs.unlinkSync('/tmp/code.out');
			}
			catch(err1){
				console.log('Error: '+err1);
			}

			_callback();
		});

}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function (req, res) {
    res.send('<b>My</b> first express http server');
});

app.route('/compile')
.post(function(req, res) {

	response["status"]="";
	response["attachment"]="";

	const code = req.body["code"];

	const lang  = req.body["language"];

	if(typeof(code) === "undefined" || typeof(lang) === "undefined"){
		res.status(400).json('Error: Check Keys of JSON object sent');
	}

	compilation(code,lang,function(){
		console.log(response);
		res.status(200).send(response);
	});

});

app.listen(3000, function () {
    console.log('Example app listening on port 3000.');
});
