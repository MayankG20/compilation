# compilation

## Introduction
So here we are building an api that can be used for compiling c or c++ code.

## Initial Need
In the problem Solving lab we are compiling codes for many of the experiments which include calling a function from python file which returns a 100 KB of html file on compilation. But on observation it 
was pointed out that the whole html page looks same before compilation and after compilation apart from message which tells whether compilation is successful or not. So instead of loading up the whole 
html file which is less efficient we want an api which on call returns the status about compilation with executable if successful or errors if not.

